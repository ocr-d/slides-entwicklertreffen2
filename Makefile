.PHONY: all

all: presentation.pdf

%.pdf: %.tex
	lualatex $*

presentation.pdf: plot-cer.tex
